import { IsNotEmpty, MinLength, IsPositive, MaxLength } from 'class-validator';
export class CreateCustomerDto {
  @MinLength(10)
  @IsNotEmpty()
  name: string;

  @IsPositive()
  @MaxLength(2)
  @IsNotEmpty()
  age: number;

  @MinLength(10)
  @IsNotEmpty()
  tel: string;

  @MaxLength(1)
  @IsNotEmpty()
  gender: string;
}
